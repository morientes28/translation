#!/bin/bash

function createConfig(){

mkdir -p $HOME/".translate"
echo "{
	\"apiKey\": \"$1\",
	\"historyPath\": \"$HOME/.translate/history\"
}"; 
}

read -p "Please, insert your Google API key: " apiKey
createConfig $apiKey > ./config/production.json;

# install node dependence
npm install

# create alias for cli command
echo " " >> ~/.bash_profile
echo "# translation cli" >> ~/.bash_profile
echo "alias sk='NODE_ENV=production node $(pwd) sk \$2'" >> ~/.bash_profile
echo "alias en='NODE_ENV=production node $(pwd) en \$2'" >> ~/.bash_profile
echo "alias it='NODE_ENV=production node $(pwd) it \$2'" >> ~/.bash_profile
echo "alias es='NODE_ENV=production node $(pwd) es \$2'" >> ~/.bash_profile

source ~/.bash_profile
