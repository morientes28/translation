'use strict';

var env = process.env.NODE_ENV || 'default';
var config = require('./config/'+ env + '.json');
const apiKey = config.apiKey || '';
const homedir = require('os').homedir();
const historyPath = config.historyPath || homedir + '/history';
const googleTranslate = require('google-translate')(apiKey);
const program = require('commander');
const fs = require('fs');

module.exports = () => {

function print(origin, translate){
	console.log('');
	console.log(origin);
	console.log('');
	console.log(translate);
	console.log('');

	fs.appendFile(historyPath, '\n---\n'+origin+'\n'+translate+'\n', function(err) {
    if(err) {
        return console.log(err);
    }
  }); 
}

function translate(lang, text){
  let translating_text = text;
  if (process.argv.length > 3){
      translating_text = process.argv.slice(3).join(' ');
  }
  googleTranslate.translate(translating_text, lang, function(err, translation) {
      print(translating_text, translation.translatedText);
  });
}

const VERSION = '0.1.0';

program
  .version(VERSION)
  .command('sk <translating_text>')
  .description('Translate text to Slovak language')
  .action(function(translating_text, optional){
    translate('sk', translating_text);
  });

program
  .version(VERSION)
  .command('en <translating_text>')
  .description('Translate text to English language')
  .action(function(translating_text, optional){
    translate('en', translating_text);
  });

program
  .version(VERSION)
  .command('it <translating_text>')
  .description('Translate text to Italian language')
  .action(function(translating_text, optional){
    translate('it', translating_text);
  });

program
  .version(VERSION)
  .command('es <translating_text>')
  .description('Translate text to Spanish language')
  .action(function(translating_text, optional){
    translate('es', translating_text);
  });

program.parse(process.argv);

}
